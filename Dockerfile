FROM node:12

RUN mkdir /home/web/

WORKDIR /home/web/

ADD ./chat-app-node/ /home/web

RUN npm install

EXPOSE 3000

CMD npm start
